import { Block, BlockTitle, Button, Input, Link, List, ListInput, Navbar, Page } from "framework7-react";
import React, { Fragment } from "react";



const Verification:React.FC = () => {

    return(
        <Fragment>
            <Page>
                <Block>
                    <BlockTitle>Вход в аккаунт</BlockTitle>
                    <Block>
                        <p>Укажите номер мобильного телефона и
                            мы пришлём СМС с кодом для входа
                        </p>
                    </Block>
                    <List>
                        <ListInput placeholder="(XXX) XXX-XX-XX">
                            <p slot='content-start'>+7</p>
                        </ListInput>
                    </List>
                    <Button fill round>Прислать СМС</Button>
                </Block>
            </Page>
        </Fragment>
    );
}
export default Verification