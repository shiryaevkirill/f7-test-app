import { Block, BlockTitle, Card, CardContent, Link, List, ListItem, NavRight, Navbar, Page, PageContent, Popup, Tab, View } from "framework7-react";
import React, { Fragment } from "react";

export default () => {
    const listItems = ['List item 1','List item 2','List item 3']
    return(
        <Fragment>
            <Navbar title="Tab 2"/>
            <Page>
                <BlockTitle>List card</BlockTitle>
                <Card>
                <CardContent padding={false}/>
                <List
                    linksList
                >
                    {listItems.map((item,index) =>
                        <ListItem key={'list-item-key-'+index}>
                                <Link
                                    text={item}
                                    popupOpen={'#list-item-popup-'+index}
                                />
                        </ListItem>
                    )}
                </List>
                </Card>
            </Page>

            {listItems.map((item,index) => 
                <Popup 
                    id={'list-item-popup-'+index}
                    key={'list-item-popup-key-'+index}
                >
                    <View router={false}>
                        <Page>
                            <Navbar title={item+" popup"}>
                                <NavRight>
                                    <Link popupClose>Close</Link>
                                </NavRight>
                            </Navbar>
                            <Block>
                                <p>{item}</p>
                            </Block>
                        </Page>
                    </View>
                </Popup>
            )}
        </Fragment>
    );
}