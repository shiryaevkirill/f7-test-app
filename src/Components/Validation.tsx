import { Block, BlockTitle, Button, Input, Link, List, ListInput, Page, f7 } from "framework7-react";
import { Fragment, ReactElement, createRef, useRef, useState } from "react";
import { forEachChild } from "typescript";

type ValidationProps = {
    number:string
}

const Validation:React.FC<ValidationProps> = ({number}) =>{

    const [code,setCode] = useState(['','','','']);
    const [currentFocus,setCurrentFocus] = useState(0)
    const [valid,setValid] = useState(true)

    const handleAcceptButton = () => {
        let concatCode = ''
        code.forEach((value)=>concatCode+=value)
        concatCode == '1234'? 
                f7.views.main.router.navigate('/adding-email') 
            :   setValid(false)
    }

    const changeFocus = (index:number,number:string)=>{
        const direction = number==''?-1:1
        setCode(code.map((value,i)=> i==index?number:value));
        setValid(true)
        document.getElementById('code-input-id-'+(currentFocus+direction))?.getElementsByTagName('input')[0].focus()
    }


    return(
        <Fragment>
            <Page>
                <BlockTitle>Введите код</BlockTitle>
                <Block>
                        <p>
                            Отправили СМС с кодом на номер
                            +7 {number}. Подтверждая номер
                            телефона, вы принимаете
                        </p>
                        <Link>Пользовательское соглашение</Link>
                    <ul 
                        className='list' 
                        style={{
                            display:'flex',
                            flexDirection:'row'
                        }}
                    >
                        {code.map((_,i)=>{
                            return(
                            <ListInput
                            placeholder="0"
                            validate
                            inputmode="numeric"
                            key={'code-input-key-'+i}
                            id={'code-input-id-'+i}
                            maxlength={'1'}
                            onInput={(e)=>{
                                changeFocus(i,e.target.value)
                            }}
                            onFocus={()=>{
                                setCurrentFocus(i)
                            }}
                            color={valid?'':'red'}
                            />);
                        })}
                    </ul>
                    <p style={{color:'red'}}>{valid?'':'Неверно введенный код. Попробуйте еще раз'}</p>
                    <Button 
                        fill
                        round
                        disabled={code.findIndex((value)=>value=='')!=-1}
                        onClick={()=>handleAcceptButton()}
                    >
                        Подтвердить
                    </Button>
                </Block>
            </Page>
        </Fragment>
    );
}

export default Validation