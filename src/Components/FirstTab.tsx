import { Block, Button, Card, CardContent, CardFooter, CardHeader, Link, NavRight, Navbar, Page, PageContent, Panel, Popup, Preloader, Tab, View } from "framework7-react";
import React, { Fragment } from "react";
import { Router } from "framework7/types";

export default () => {
    const cardsData = ['Card 1','Card 2','Card 3']
    return(
        <Fragment>
            <Navbar title="Tab 1"/>
            <Page>
                {cardsData.map((card,i)=>
                    <Card key={'tab-1-card-'+i}>
                        <CardHeader
                            style={{
                                backgroundImage: `url(https://cdn.framework7.io/placeholder/nature-1000x600-${i+1}.jpg)`,
                                backgroundSize: 'contain',
                                backgroundRepeat: 'no-repeat',
                                width: '100%',
                                height: 0,
                                paddingTop: '66%',
                            }}
                        >
                            {card}
                        </CardHeader>
                        <CardContent>
                            <p>{card} text</p>
                        </CardContent>
                        <CardFooter>
                            <Link 
                                href={'/tab-1/'+(i+1)+'/'}
                                view='#tab-1'
                            >
                                Read more
                            </Link>
                        </CardFooter>
                    </Card>
                )}
                <Block>
                    <Button 
                        href='/tab-1/about' 
                        view="#tab-1"                        
                        text='About'
                        large
                        fillIos
                        fillMd
                    />
                </Block>
            </Page>
        </Fragment>
    )
}