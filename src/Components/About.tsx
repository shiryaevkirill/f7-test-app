import { Block, Link, NavLeft, Navbar, Page, PageContent } from "framework7-react";
import React, { Fragment } from "react";

export default () => {
    return(
        <Fragment>
            <Navbar title="About">
                <Link back slot='left'>Back</Link>
            </Navbar>
            <Page>
                <Block>
                    <p>About</p>
                </Block>
            </Page>
        </Fragment>
    );
}