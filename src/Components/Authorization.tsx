import { Keyboard } from "@capacitor/keyboard";
import { Block, BlockTitle, Button, Input, Link, List, ListInput, Navbar, Page, f7 } from "framework7-react";
import React, { Fragment, useEffect, useState } from "react";



const Authorization:React.FC = () => {

    const [phoneNumber, setPhoneNumber] = useState("");

    const handleInput = (number:string) => {
        setPhoneNumber(number);
    }

    const handleAcceptButton = () => {
        f7.views.main.router.navigate('/Validation',{props: {number: phoneNumber}})
    }

    return(
        <Fragment>
            <Page>
                <BlockTitle>Вход в аккаунт</BlockTitle>
                <Block>
                        <p>Укажите номер мобильного телефона и
                            мы пришлём СМС с кодом для входа
                        </p>
                    <List>
                        <ListInput 
                            placeholder="(XXX) XXX-XX-XX"
                            validate
                            inputmode="numeric"
                            autofocus
                            onInput={(e)=>handleInput(e.target.value)}
                            maxlength={'10'}
                        >
                            <p slot='content-start'>+7</p>
                        </ListInput>
                    </List>
                    <Button 
                        fill
                        round
                        disabled={phoneNumber.length!=10}
                        onClick={()=>handleAcceptButton()}
                    >Прислать СМС
                    </Button>
                </Block>
            </Page>
        </Fragment>
    );
}
export default Authorization