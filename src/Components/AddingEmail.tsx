import { Block, BlockTitle, Button, List, ListInput, Page, f7 } from "framework7-react";
import { Fragment, useContext, useState } from "react";
import { AppContext } from "../App";

const AddingEmail:React.FC = () =>{
    const context = useContext(AppContext)
    const [valid,setValid] = useState(false);

    const handleEnterEmailButton=()=>{
        context.setAuthorized && context.setAuthorized(true)
        f7.views.main.router.navigate('/')
        f7.views.main.router.navigate('#tab-1')
    }

    return(
        <Fragment>
            <Page>
                <BlockTitle>
                    Добавьте email
                </BlockTitle>
                <Block>
                    <p>
                        На него мы отправляем
                        электронные чеки и квитанции
                    </p>
                    <List>
                        <ListInput
                            type='email'
                            validate
                            onValidate={(val:boolean)=>setValid(val)}
                            placeholder="example@email.com"
                        />
                    </List>
                    <Button
                        fill
                        round
                        // tabLink
                        // href='#tab-1'
                        disabled={!valid}
                        onClick={()=>handleEnterEmailButton()}
                    >
                        Далее
                    </Button>
                </Block>
            </Page>
        </Fragment>
    );
}

export default AddingEmail