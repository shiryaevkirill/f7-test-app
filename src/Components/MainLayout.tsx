import { Link, Toolbar, View, Views } from "framework7-react";
import { Fragment } from "react";

type MainLaoutProps = {
    authorized: boolean
}

const MainLayout:React.FC<MainLaoutProps> = ({authorized}) =>{
    return(
        <Fragment>
        <Views tabs>
        <Toolbar 
            bottom 
            tabbar
            icons
        >
        <Link
            tabLink
            href='#tab-1'
            text = 'Tab 1'
            iconIos="f7:chat_bubble_fill"
            iconMd="material:chat"
            tabLinkActive
          />
          <Link
            tabLink
            href='#tab-2'
            text = 'Tab 2'
            iconIos="f7:person_2_alt"
            iconMd="material:chat"
          />
          <Link
            tabLink='#tab-3'
            text = 'Tab 3'
            iconIos="f7:archivebox_fill"
            iconMd="material:chat"
          />
        </Toolbar>
        <View tab id='tab-1' url='/tab-1/firstTab' tabActive />
        <View tab id='tab-2' url='/tab-2/secondTab'/>
        <View tab id='tab-3' url='/tab-3/settings'/>
        </Views>
        </Fragment>
    );
}

export default MainLayout
