import { Block, Link, List, ListItem, Navbar, Page, Toggle, f7 } from "framework7-react";
import { Fragment, useState } from "react";

export default () => {

    const [theme, setTheme] = useState('light');

    const setScheme = (newTheme:string) => {
        f7.setDarkMode(newTheme === 'dark');
        setTheme(newTheme)
      };

    return(
        <Fragment>
            <Navbar title="Settings"/>
            <Page>
            <List simpleList strong outlineIos dividersIos>
                <ListItem>
                    <span>Dark theme</span>
                    <Toggle onChange={() => theme=='light'?setScheme('dark'):setScheme('light')}/>
                </ListItem>
            </List>
            </Page>
        </Fragment>
    );
}