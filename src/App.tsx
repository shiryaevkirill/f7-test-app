import { App, View, Page, Navbar, Toolbar, Link, Panel, Block, Button, Popup, NavRight, Preloader, Icon, Tabs, Views, Tab } from 'framework7-react';
import { createContext, useEffect, useState } from 'react';

import SecondTab from './Components/SecondTab';
import FirstTab from './Components/FirstTab';
import About from './Components/About';
import ReadMoreHolder from './Components/ReadMoreHolder';
import Settings from './Components/Settings';
import Authorization from './Components/Authorization';
import Validation from './Components/Validation';
import AddingEmail from './Components/AddingEmail';
import MainLayout from './Components/MainLayout';

type ContextType = {
  setAuthorized?: (auth:boolean) => void;
}

const AppContext:React.Context<ContextType> = createContext({});


const f7params = {
  name: 'My App',
  theme: 'auto',
  routes: [
    {
      path: '/',
      component: MainLayout
    },
    {
      path: '/authorization',
      component: Authorization
      
    },
    {
      path: '/validation',
      component: Validation
    },
    {
      path: '/adding-email',
      component: AddingEmail
    },
    {
      path: '/tab-1/about',
      component: About
    },
    {
      path:'/tab-1/firstTab',
      component: FirstTab
    },
    {
      path:'/tab-2/secondTab',
      component: SecondTab,
    },
    {
      path: '/tab-1/:pageId/',
      component: ReadMoreHolder,
    },
    {
      path: '/tab-3/settings',
      component: Settings,
    }
  ]
  // ...
};

export {AppContext}
export default () => {
  
  const [authorized,setAuthorized] = useState(false)

  return(
  <App {...f7params}>
      <AppContext.Provider value={{setAuthorized: setAuthorized}}>
        <View 
          url='/authorization/' 
          main={!authorized}
        />
        <View 
          main={authorized} 
          url='/'/>
      </AppContext.Provider>
  </App>
)}