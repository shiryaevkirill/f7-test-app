import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.example.app',
  appName: 'f7-test-app',
  webDir: 'build',
  server: {
    url: 'http://192.168.1.104:3000'
  }
};

export default config;
